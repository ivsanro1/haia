﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GeneticSharp.Domain;

public class Starting_Window_Script : MonoBehaviour {

    public Text population_size_text;
    public Slider population_size_slider;
    public Canvas starting_window_canvas;
    public Button starting_window_button;
    public Canvas problem_setup_window;

    
    



    // Use this for initialization
    void Start () {

        //GeneticAlgorithm myGeneticAlgorithm = new GeneticAlgorithm();

        population_size_slider.onValueChanged.AddListener(delegate { ValueChangeAction(); });
        starting_window_button.onClick.AddListener(delegate { starting_window_button_Clicked_Action(); });
        
        population_size_text.text = "Population size: 50";
    }
	
	// Update is called once per frame
	void Update () {
        
        
    }

    public void ValueChangeAction()
    {
        population_size_text.text = "Population size: " + population_size_slider.value;
    }

    public void starting_window_button_Clicked_Action()
    {
        starting_window_canvas.enabled = false;
        problem_setup_window.enabled = true;
    }

}
