﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class Rectangle
{
    private int id;
    public float x, y, w, h;

    public Rectangle(float x, float y, float w, float h)
    {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public void set_id(int id)
    {
        this.id = id;
    }

    public int get_id()
    {
        return id;
    }

    public Boolean fits_in(Rectangle outer)
    {
        return outer.w >= this.w && outer.h >= this.h;
    }

    public Boolean same_size_as(Rectangle other)
    {
        return this.w == other.w && this.h == other.h;
    }



}

public class Node
{
    public Node left;
    public Node right;
    public Rectangle rect;
    public Boolean filled;


    public Node()
    {
        this.left = null;
        this.right = null;
        this.rect = null;
        this.filled = false;
    }
       
    public Node insert_rect(Rectangle rect)
    {
        

        // if it is not a leaf, then
        if(this.left != null)
        {
            Node newNode = this.left.insert_rect(rect);
            // try insert into first child
            if (newNode != null)
            {
                return newNode;
            }

            else return this.right.insert_rect(rect);
        }

        if (this.filled)
            return null;

        if (!rect.fits_in(this.rect))
            return null;

        if (rect.same_size_as(this.rect))
        {
            // same size means it's filled when inserted
            this.filled = true;
            return this;
        }

        // When a call to this methods ends up here, it means that has get to a leaf, and this one must branch:

        // Branch
        this.left = new Node();
        this.right = new Node();

        float width_diff = this.rect.w - rect.w;
        float height_diff = this.rect.h - rect.h;

        var me = this.rect;
        
        if (width_diff > height_diff)
        {
            // split literally into left and right, putting the rect on the left.
            Debug.Log("Split into left and right. Putting the rect on the left");
            this.left.rect = new Rectangle(me.x, me.y, rect.w, me.h);
            this.right.rect = new Rectangle(me.x + rect.w, me.y, me.w - rect.w, me.h);
            
        }
        else
        {
            // split into top and bottom, putting rect on top.
            Debug.Log("Split into top and bottom, putting rect on top");
            this.left.rect = new Rectangle(me.x, me.y, me.w, rect.h);
            this.right.rect = new Rectangle(me.x, me.y - rect.h, me.w, me.h - rect.h);
        }

        return this.left.insert_rect(rect);

    } 

}

public class App_Script : MonoBehaviour
{
    private const float MIN_PIECE_HEIGHT = 25;
    private const float MAX_PIECE_HEIGHT = 125;

    private const float MIN_PIECE_WIDTH = 25;
    private const float MAX_PIECE_WIDTH = 125;


    private const float MIN_SHEET_HEIGHT = 200;
    private const float MAX_SHEET_HEIGHT = 420;

    private const float MIN_SHEET_WIDTH = 320;
    private const float MAX_SHEET_WIDTH = 600;


    




    // Starting window elements
    public Text text_population_size;
    public Slider slider_population_size;
    public Canvas canvas_starting_window;
    public Button button_starting_window;
    public Canvas canvas_problem_setup_window;



    // Problem setup window elements


    // Piece related elements
    public GameObject panel_piece_canvas;

    public Scrollbar scrollbar_piece_width;
    public Scrollbar scrollbar_piece_height;
    public GameObject panel_piece;
    public InputField inputField_piece_height;
    public InputField inputField_piece_width;

    public Text text_restriction_comment_height;
    public Text text_restriction_comment_width;

    public Text text_piece_area;

    public Slider slider_RGB_R;
    public Slider slider_RGB_G;
    public Slider slider_RGB_B;

    public Button button_add_piece;
    public Button button_example;


    // Sheet related elements
    public GameObject panel_canvas_sheet;
    public GameObject panel_sheet;
    public Scrollbar scrollbar_canvas_sheet_width;
    public Scrollbar scrollbar_canvas_sheet_height;


    public InputField inputField_sheet_height;
    public InputField inputField_sheet_width;

    public Text text_restriction_comment_height_sheet;
    public Text text_restriction_comment_width_sheet;

    public Text text_sheet_area;

    public Text text_sheet_remaining_capacity;

    public Text text_warning;
    public Button button_help_at_warning;
    public GameObject panel_warning_and_help;

    public Button button_sheet_clear;
    public Button button_sheet_rearrange;



    // Virtual list of pieces added to the sheet
    public List<GameObject> list_pieces = new List<GameObject>();

    // Current dimensions of the sheet
    public float current_sheet_width;
    public float current_sheet_height;

    // Starting node representing the whole sheet space
    Node start_node = new Node();


    private bool automatic_rearrange_upon_resize = true;



    // Use this for initialization
    void Start()
    {
        // Set screen resolution for UI consistency purposes
        Screen.SetResolution(1210, 658, false);

        
        // Starting window (text) setup
        text_population_size.text = "Population size: 50";

        
        // Starting window delegates
        slider_population_size.onValueChanged.AddListener(delegate { ValueChangeAction(); });
        button_starting_window.onClick.AddListener(delegate { starting_window_button_Clicked_Action(); });

        
        // Problem setup window delegates
        scrollbar_piece_height.onValueChanged.AddListener(delegate { scrollbar_piece_height_onValueChanged(); });
        scrollbar_piece_width.onValueChanged.AddListener(delegate { scrollbar_piece_width_onValueChanged(); });
        inputField_piece_height.onEndEdit.AddListener(delegate { inputField_piece_height_onEndEdit(); });
        inputField_piece_width.onEndEdit.AddListener(delegate { inputField_piece_width_onEndEdit(); });

        // Add piece button delegate
        button_add_piece.onClick.AddListener(delegate { button_add_piece_onClick(); });
        button_example.onClick.AddListener(delegate { button_example_onClick(); });

        // Scrollbar of canvas of the sheet delegates
        scrollbar_canvas_sheet_height.onValueChanged.AddListener(delegate { scrollbar_canvas_sheet_height_onValueChanged(); });
        scrollbar_canvas_sheet_width.onValueChanged.AddListener(delegate { scrollbar_canvas_sheet_width_onValueChanged(); });

        // RGB sliders delegates
        slider_RGB_R.onValueChanged.AddListener(delegate { slider_RGB_R_onValueChanged(); });
        slider_RGB_G.onValueChanged.AddListener(delegate { slider_RGB_G_onValueChanged(); });
        slider_RGB_B.onValueChanged.AddListener(delegate { slider_RGB_B_onValueChanged(); });


        // Sheet InputField delegates
        inputField_sheet_height.onEndEdit.AddListener(delegate { inputField_sheet_height_onEndEdit(); });
        inputField_sheet_width.onEndEdit.AddListener(delegate { inputField_sheet_width_onEndEdit(); });

        // Sheet clear button delegate
        button_sheet_clear.onClick.AddListener(delegate { button_sheet_clear_onClick(); });

        // Sheet rearrange button delegate
        button_sheet_rearrange.onClick.AddListener(delegate { button_sheet_rearrange_onClick(); });

        

        // Setup the starting text inside the piece's InputFields
        inputField_piece_height.text = MIN_PIECE_HEIGHT.ToString();
        inputField_piece_width.text = MIN_PIECE_WIDTH.ToString();

        // Setup the starting text inside the sheet's InputFields
        inputField_sheet_height.text = MAX_SHEET_HEIGHT.ToString();
        inputField_sheet_width.text = MAX_SHEET_WIDTH.ToString();

        // Setup the restriction comments accordingly to the restrictions
        text_restriction_comment_height.text = "Between " + MIN_PIECE_HEIGHT + " and " + MAX_PIECE_HEIGHT;
        text_restriction_comment_width.text = "Between " + MIN_PIECE_WIDTH + " and " + MAX_PIECE_WIDTH;

        text_restriction_comment_height_sheet.text = "Between " + MIN_SHEET_HEIGHT + " and " + MAX_SHEET_HEIGHT;
        text_restriction_comment_width_sheet.text = "Between " + MIN_SHEET_WIDTH + " and " + MAX_SHEET_WIDTH;

        // Area text of piece set to MIN_HEIGHT*MIN_WIDTH
        text_piece_area.text = "Area: " + MIN_PIECE_HEIGHT * MIN_PIECE_WIDTH;


        panel_warning_and_help.SetActive(false);
        text_warning.text = "Warning: There are one or more pieces that do not fit in the current conditions";
        

        // Area text set of sheet set to MAX*MAX
        text_sheet_remaining_capacity.text = "Capacity: " + MAX_SHEET_HEIGHT * MAX_SHEET_WIDTH + " / " + MAX_SHEET_HEIGHT * MAX_SHEET_WIDTH + "  ( 100.00 % )";

        // Problem setup window objects' size, depending on pieces' max/min possible size
        panel_piece_canvas.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_PIECE_WIDTH, MAX_PIECE_HEIGHT);
        scrollbar_piece_width.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_PIECE_WIDTH, 10);
        scrollbar_piece_height.GetComponent<RectTransform>().sizeDelta = new Vector2(10, MAX_PIECE_HEIGHT);

        // Change scrollbars of canvas sheet depending of its size
        scrollbar_canvas_sheet_width.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_SHEET_WIDTH - MIN_SHEET_WIDTH+30, 10);
        scrollbar_canvas_sheet_height.GetComponent<RectTransform>().sizeDelta = new Vector2(10, MAX_SHEET_HEIGHT - MIN_SHEET_HEIGHT+20);

        panel_canvas_sheet.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_SHEET_WIDTH+15, MAX_SHEET_HEIGHT+15);

        panel_sheet.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_SHEET_WIDTH, MAX_SHEET_HEIGHT);



        
        panel_sheet.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_SHEET_WIDTH, MAX_SHEET_HEIGHT);

        // Set up the virtual calculations for the rectangles:

        // The rectangle of the starting node is the one representing the whole sheet. It will change if the sheet redimensionates
        start_node.rect = new Rectangle(0, 0, panel_sheet.GetComponent<RectTransform>().rect.width, panel_sheet.GetComponent<RectTransform>().rect.height);



        // Start the value of the piece to its minimum
        panel_piece.GetComponent<RectTransform>().sizeDelta = new Vector2(MIN_PIECE_WIDTH, MIN_PIECE_HEIGHT);


        // Set current dimensions of the sheet to its maximum
        current_sheet_width = MAX_SHEET_WIDTH;
        current_sheet_height = MAX_SHEET_HEIGHT;

        // Start the value of the sheet to its maximum
        panel_sheet.GetComponent<RectTransform>().sizeDelta = new Vector2(MAX_SHEET_WIDTH, MAX_SHEET_HEIGHT);

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Starting window Delegated methods 
    public void ValueChangeAction()
    {
        text_population_size.text = "Population size: " + slider_population_size.value;
    }

    public void starting_window_button_Clicked_Action()
    {
        canvas_starting_window.enabled = false;
        canvas_problem_setup_window.enabled = true;
    }


    // Problem setup window Delegated methods
    public void scrollbar_piece_height_onValueChanged()
    {
        // Change value of InputField
        float proportion = MIN_PIECE_HEIGHT / MAX_PIECE_HEIGHT;

        float normalized_value = (scrollbar_piece_height.value * (1 - proportion) + proportion) * MAX_PIECE_HEIGHT;


        inputField_piece_height.text = normalized_value.ToString();

        // and change height of the piece
        RectTransform panel_piece_RectTransform = panel_piece.GetComponent<RectTransform>();

        panel_piece_RectTransform.sizeDelta = new Vector2(panel_piece_RectTransform.rect.width, normalized_value);

        // Recalculate area and change text
        text_piece_area.text = "Area: " + panel_piece_RectTransform.rect.width * normalized_value;
    }



    public void scrollbar_piece_width_onValueChanged()
    {
        // Change value of InputField
        float proportion = MIN_PIECE_WIDTH / MAX_PIECE_WIDTH;
        float normalized_value = (scrollbar_piece_width.value * (1 - proportion) + proportion) * MAX_PIECE_WIDTH;
        inputField_piece_width.text = normalized_value.ToString();

        // and change width of the piece
        RectTransform panel_piece_RectTransform = panel_piece.GetComponent<RectTransform>();

        panel_piece_RectTransform.sizeDelta = new Vector2(normalized_value, panel_piece_RectTransform.rect.height);

        // Recalculate area and change text
        text_piece_area.text = "Area: " + normalized_value * panel_piece_RectTransform.rect.height;
    }



    public void inputField_piece_height_onEndEdit()
    {
        try
        {
            int value = Convert.ToInt32(inputField_piece_height.text);

            if (value < MIN_PIECE_HEIGHT)
            {
                value = (int) MIN_PIECE_HEIGHT;
                inputField_piece_height.text = MIN_PIECE_HEIGHT.ToString();
            }

            if (value > MAX_PIECE_HEIGHT)
            {
                value = (int) MAX_PIECE_HEIGHT;
                inputField_piece_height.text = MAX_PIECE_HEIGHT.ToString();
            }

            // Change scrollbar position according to value
            scrollbar_piece_height.value = ((float)value - MIN_PIECE_HEIGHT)/(MAX_PIECE_HEIGHT-MIN_PIECE_HEIGHT);

            // Change piece size according to value
            RectTransform panel_piece_RectTransform = panel_piece.GetComponent<RectTransform>();
            panel_piece_RectTransform.sizeDelta = new Vector2(panel_piece_RectTransform.rect.width, value);
        }

        catch (Exception)
        {
            Console.WriteLine("El valor introducido no es válido");
        }
    }


    public void inputField_piece_width_onEndEdit()
    {
        try
        {
            int value = Convert.ToInt32(inputField_piece_width.text);

            if (value < MIN_PIECE_WIDTH)
            {
                value = (int)MIN_PIECE_WIDTH;
                inputField_piece_width.text = MIN_PIECE_WIDTH.ToString();

            }

            if (value > MAX_PIECE_WIDTH)
            {
                value = (int)MAX_PIECE_WIDTH;
                inputField_piece_width.text = MAX_PIECE_WIDTH.ToString();
            }

            // Change scrollbar position according to value
            scrollbar_piece_width.value = ((float)value - MIN_PIECE_WIDTH) / (MAX_PIECE_WIDTH - MIN_PIECE_WIDTH);

            // Change piece size according to value
            RectTransform panel_piece_RectTransform = panel_piece.GetComponent<RectTransform>();
            panel_piece_RectTransform.sizeDelta = new Vector2(value, panel_piece_RectTransform.rect.height);
        }

        catch (Exception)
        {
            Console.WriteLine("El valor introducido no es válido");
        }
    }



    public void inputField_sheet_height_onEndEdit()
    {
        try
        {
            int value = Convert.ToInt32(inputField_sheet_height.text);

            if (value < MIN_SHEET_HEIGHT)
            {
                value = (int)MIN_SHEET_HEIGHT;
                inputField_sheet_height.text = MIN_SHEET_HEIGHT.ToString();
            }

            if (value > MAX_SHEET_HEIGHT)
            {
                value = (int)MAX_SHEET_HEIGHT;
                inputField_sheet_height.text = MAX_SHEET_HEIGHT.ToString();
            }

            // Change scrollbar position according to value
            scrollbar_canvas_sheet_height.value = ((float)value - MIN_SHEET_HEIGHT) / (MAX_SHEET_HEIGHT - MIN_SHEET_HEIGHT);

            // Change sheet size according to value
            RectTransform panel_sheet_RectTransform = panel_sheet.GetComponent<RectTransform>();
            panel_sheet_RectTransform.sizeDelta = new Vector2(panel_sheet_RectTransform.rect.width, value);

            
        }

        catch (Exception)
        {
            Console.WriteLine("El valor introducido no es válido");
        }
    }


    public void inputField_sheet_width_onEndEdit()
    {
        try
        {
            int value = Convert.ToInt32(inputField_sheet_width.text);

            if (value < MIN_SHEET_WIDTH)
            {
                value = (int)MIN_SHEET_WIDTH;
                inputField_sheet_width.text = MIN_SHEET_WIDTH.ToString();

            }

            if (value > MAX_SHEET_WIDTH)
            {
                value = (int)MAX_SHEET_WIDTH;
                inputField_sheet_width.text = MAX_SHEET_WIDTH.ToString();
            }

            // Change scrollbar position according to value
            scrollbar_canvas_sheet_width.value = ((float)value - MIN_SHEET_WIDTH) / (MAX_SHEET_WIDTH - MIN_SHEET_WIDTH);

            // Change sheet size according to value
            RectTransform panel_sheet_RectTransform = panel_sheet.GetComponent<RectTransform>();
            panel_sheet_RectTransform.sizeDelta = new Vector2(value, panel_sheet_RectTransform.rect.height);
        }

        catch (Exception)
        {
            Console.WriteLine("El valor introducido no es válido");
        }
    }


    public void scrollbar_canvas_sheet_height_onValueChanged()
    {
        RectTransform panel_sheet_RectTransform = panel_sheet.GetComponent<RectTransform>();
        
        float proportion = MIN_SHEET_HEIGHT / MAX_SHEET_HEIGHT;
        float normalized_value = ((scrollbar_canvas_sheet_height.value * (1 - proportion) + proportion) * MAX_SHEET_HEIGHT);

        // Change value of InputField
        inputField_sheet_height.text = normalized_value.ToString();

        // change height of the sheet
        panel_sheet_RectTransform.sizeDelta = new Vector2(panel_sheet_RectTransform.rect.width, normalized_value);

        // change height of the rect in node
        start_node.rect = new Rectangle(0, 0, panel_sheet.GetComponent<RectTransform>().rect.width, normalized_value);
        
        // Change value of current sheet height
        current_sheet_height = normalized_value;


        // change text of remaining capacity:
        update_capacity_text();

        

        // change text of total sheet area

        if(automatic_rearrange_upon_resize) rearrange();


    }

    public void scrollbar_canvas_sheet_width_onValueChanged()
    {
        RectTransform panel_sheet_RectTransform = panel_sheet.GetComponent<RectTransform>();

        float proportion = MIN_SHEET_WIDTH / MAX_SHEET_WIDTH;
        float normalized_value = ((scrollbar_canvas_sheet_width.value * (1 - proportion) + proportion) * MAX_SHEET_WIDTH);

        // Change value of InputField
        inputField_sheet_width.text = normalized_value.ToString();
        
        // change height of the sheet
        panel_sheet_RectTransform.sizeDelta = new Vector2(normalized_value, panel_sheet_RectTransform.rect.height);

        // Change value of current sheet height
        current_sheet_width = normalized_value;

        // change text of remaining capacity:
        update_capacity_text();

        // change text of total sheet area
        if (automatic_rearrange_upon_resize) rearrange();

    }

    public void button_add_piece_onClick()
    {
        

        // Make another instance of the piece to be added
        Debug.Log("Size of sheet canvas -> Width:" + panel_sheet.GetComponent<RectTransform>().rect.width + "... Height: " + panel_sheet.GetComponent<RectTransform>().rect.height);
        GameObject piece_to_add = Instantiate(panel_piece);
        piece_to_add.transform.SetParent(panel_sheet.transform);

        set_tag_to_piece(piece_to_add, list_pieces.Count + 1);
        


        // Create virtual rectangle
        Rectangle piece_to_add_RECT = new Rectangle(0, 0, piece_to_add.GetComponent<RectTransform>().rect.width, piece_to_add.GetComponent<RectTransform>().rect.height);

        Node node = start_node.insert_rect(piece_to_add_RECT);


        if (node != null)
        {
            Rectangle r = node.rect;
            piece_to_add.transform.localPosition = new Vector2(r.x, r.y);

            

            // The piece must be only added if it's added to the tree
            list_pieces.Add(piece_to_add);

            Debug.Log("The added piece has its starting point at: ( " + r.x + ", " + r.y + "), its upper-right corner at " + get_upper_right_position(piece_to_add) + ", its lower-left corner at " + get_lower_left_position(piece_to_add) + ", and its lower-right corner at " + get_lower_right_position(piece_to_add));
        }

        update_capacity_text();

    }


    public void button_example_onClick()
    {
        List<Vector2> pieces_dimensions = new List<Vector2>{
                                                             new Vector2(40, 60),   // piece 1
                                                             new Vector2(40, 60),   // piece 2
                                                             new Vector2(40, 60),   // piece 3
                                                             new Vector2(40, 60),   // piece 4
                                                             new Vector2(40, 60),   // piece 5
                                                             new Vector2(40, 60),   // piece 6
                                                             new Vector2(70, 110),  // piece 7
                                                             new Vector2(80, 110),  // piece 8
                                                             new Vector2(90, 40),   // piece 9
                                                             new Vector2(90, 30),   // piece 10
                                                             new Vector2(90, 40),   // piece 11
                                                             new Vector2(90, 80),   // piece 12
                                                             new Vector2(100, 80),  // piece 13
                                                             new Vector2(50, 80),   // piece 14
                                                             new Vector2(50, 40),   // piece 15
                                                             new Vector2(50, 60),   // piece 16
                                                             new Vector2(100, 100), // piece 17
                                                             new Vector2(90, 60),   // piece 18
                                                             new Vector2(90, 40),   // piece 19
                                                             new Vector2(70, 70),   // piece 20
                                                             new Vector2(90, 70),   // piece 21
                                                             new Vector2(80, 70)    // piece 22
                                                             };

        for (int i = 0; i < pieces_dimensions.Count; i++)
        {
            GameObject piece_to_add = Instantiate(panel_piece);
            piece_to_add.transform.SetParent(panel_sheet.transform);
            piece_to_add.GetComponent<RectTransform>().sizeDelta = pieces_dimensions[i];

            set_tag_to_piece(piece_to_add, list_pieces.Count + 1);



            // Create virtual rectangle
            Rectangle piece_to_add_RECT = new Rectangle(0, 0, pieces_dimensions[i].x, pieces_dimensions[i].y);

            Node node = start_node.insert_rect(piece_to_add_RECT);


            if (node != null)
            {
                Rectangle r = node.rect;
                piece_to_add.transform.localPosition = new Vector2(r.x, r.y);



                // The piece must be only added if it's added to the tree
                list_pieces.Add(piece_to_add);

                Debug.Log("The added piece has its starting point at: ( " + r.x + ", " + r.y + "), its upper-right corner at " + get_upper_right_position(piece_to_add) + ", its lower-left corner at " + get_lower_left_position(piece_to_add) + ", and its lower-right corner at " + get_lower_right_position(piece_to_add));
            }

            update_capacity_text();
        }
    }



    // Gets the point (relative coordinates to GameObject's parent) of the upper left corner, i.e, (x, y)
    public Vector2 get_upper_left_position(GameObject piece)
    {
        return new Vector2(piece.transform.localPosition.x, piece.transform.localPosition.y);
    }

    public Vector2 get_upper_right_position(GameObject piece)
    {
        return new Vector2(piece.transform.localPosition.x + piece.GetComponent<RectTransform>().rect.width, piece.transform.localPosition.y);
    }

    public Vector2 get_lower_left_position(GameObject piece)
    {
        return new Vector2(piece.transform.localPosition.x, piece.transform.localPosition.y - piece.GetComponent<RectTransform>().rect.height);
    }

    public Vector2 get_lower_right_position(GameObject piece)
    {
        return new Vector2(piece.transform.localPosition.x + piece.GetComponent<RectTransform>().rect.width, piece.transform.localPosition.y - piece.GetComponent<RectTransform>().rect.height);
    }





    public void slider_RGB_R_onValueChanged()
    {
        Image panel = panel_piece.GetComponent<Image>();
        panel.color = new Color(slider_RGB_R.value, panel.color.g, panel.color.b);
    }

    public void slider_RGB_G_onValueChanged()
    {
        Image panel = panel_piece.GetComponent<Image>();
        panel.color = new Color(panel.color.r, slider_RGB_G.value, panel.color.b);
    }

    public void slider_RGB_B_onValueChanged()
    {
        Image panel = panel_piece.GetComponent<Image>();
        panel.color = new Color(panel.color.r, panel.color.g, slider_RGB_B.value);
    }


    public void update_capacity_text()
    {
        float total_current_area = current_sheet_width * current_sheet_height;
        float pieces_area = 0;

        foreach(GameObject piece in list_pieces)
        {
            pieces_area += piece.GetComponent<RectTransform>().rect.width * piece.GetComponent<RectTransform>().rect.height;
        }

        float remaining_area = total_current_area - pieces_area;
        float percentage = remaining_area / total_current_area;

        text_sheet_remaining_capacity.text = "Capacity: " + remaining_area + " / " + total_current_area + "  ( " + String.Format("{0:0.00}", percentage*100) + " % )";
    }

    public void rearrange()
    {
        bool all_pieces_have_been_put = true;

        clear_sheet();

        foreach (GameObject piece in list_pieces)
        {

            // Create virtual rectangle
            Rectangle piece_to_add_RECT = new Rectangle(0, 0, piece.GetComponent<RectTransform>().rect.width, piece.GetComponent<RectTransform>().rect.height);

            Node node = start_node.insert_rect(piece_to_add_RECT);


            if (node != null)
            {
                Rectangle r = node.rect;
                piece.transform.localPosition = new Vector2(r.x, r.y);
                piece.SetActive(true);
            }

            else
            {
                panel_warning_and_help.SetActive(true);
                piece.SetActive(false);
                all_pieces_have_been_put = false;
            }

            if (all_pieces_have_been_put) panel_warning_and_help.SetActive(false);




            }

    }


    public void clear_sheet()
    {
        foreach (GameObject piece in list_pieces)
        {
            // Disable pieces from sheet
            piece.SetActive(false);
        }

        // Restart the tree that represents the space
        start_node = new Node();
        // Reinitialize the space in that node with the current sheet proportions
        start_node.rect = new Rectangle(0, 0, panel_sheet.GetComponent<RectTransform>().rect.width, panel_sheet.GetComponent<RectTransform>().rect.height);
        panel_warning_and_help.SetActive(false);
    }

    public void button_sheet_clear_onClick()
    {
        clear_sheet();
        list_pieces.Clear();
    }

    public void button_sheet_rearrange_onClick()
    {
        rearrange();
    }


    public void set_tag_to_piece(GameObject piece_to_add, int id)
    {
        // Create text to identify the piece
        GameObject text_piece_id = new GameObject();
        text_piece_id.AddComponent<Text>();
        // Set size as the piece
        text_piece_id.GetComponent<RectTransform>().sizeDelta = new Vector2(piece_to_add.GetComponent<RectTransform>().rect.width, piece_to_add.GetComponent<RectTransform>().rect.height);
        text_piece_id.GetComponent<Text>().text = id.ToString();
        text_piece_id.GetComponent<Text>().font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        text_piece_id.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
        text_piece_id.transform.SetParent(piece_to_add.transform);
        // Set position as the piece so they perfectly overlap
        text_piece_id.transform.localPosition = new Vector2(piece_to_add.GetComponent<RectTransform>().rect.width / 2, -piece_to_add.GetComponent<RectTransform>().rect.height / 2);
    }


}
